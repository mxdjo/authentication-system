<?php
// On se connecte à la bdd
include('../config/database.php'); 

// Vérification de la validité des informations
if(isset($_POST['register']) AND $_POST['cs'] == "CRD5-GTFT-CK65-JOPM-V29N-24G1-HH28-LLFV"){

//Si le formulaire a été soumis (register) et le code secret est égal à ...........     

//Si tous les champs ne sont pas vides

if(!empty(['name', 'pseudo', 'email', 'pass', 'password_confirm'])){
	//Si tous les champs sont remplis
   // On créé des variables
   
   extract($_POST);
   // Hachage du mot de passe
   
   $pass_hache = sha1($_POST['pass']);
   // Insertion dans la bdd
   
   $req = $db->prepare('INSERT INTO users(name, pseudo, email, pass, date_inscription) VALUES (:name, :pseudo, :email, :pass, CURDATE())');
   $req-> execute(array(
	   'name' => $name,
	   'pseudo' => $pseudo,
	   'email' => $email,
	   'pass' => $pass_hache
   ));

}

}
?>