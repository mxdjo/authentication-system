<?php 

	// On se connecte à la bdd
	include('../config/database.php');
	
	//Si le formulaire a été soumis (login) et le code secret est juste
	if(isset($_POST['login'])){
		 
	 //Si tous les champs sont remplis (pseudo et pass)
		if(!empty(['pseudo', 'pass'])){
			// On créé des variables ...
				extract($_POST);
			// Hachage du mot de passe
				$pass_hache = sha1($pass);

			// Vérification des identifiants (SELECT)
				$req = $db->prepare('SELECT id FROM users WHERE pseudo = :pseudo AND pass = :pass');
				$req->execute(
					array(
						'pseudo' => $pseudo,
						'pass' => $pass_hache
					)
				);
			// Le résultat est égal à la récupération des données
				$resultat = $req->fetch();
			// Si c'est différent de résultat
			if (!$resultat)
			{
				echo '
				<html>
				<head>
					<meta charset="utf-8"/>
					<title>Xfiles</title>
					<!-- Latest compiled and minified CSS -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
					<!-- Optional theme -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
				</head>
				<body>
				<br/>
				<div class="well col-md-6 col-md-offset-3">
				<p>Mauvais identifiant ou mot de passe !</p>
				</div>
				<!-- Latest compiled and minified JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				</body>
				</html>
				';
				header('Refresh:2;url=../connexion.php');
			}
            //sinon
			else
			{
				// On démarre la session
				session_start();
				// la séssion id correspond au résultat précédent
				$_SESSION['id'] = $resultat['id'];
				// la session pseudo correspond à la variable pseudo
				$_SESSION['pseudo'] = $pseudo;
				// et on affiche : 
				echo '
				<html>
				<head>
					<meta charset="utf-8"/>
					<title>Xfiles</title>
					<!-- Latest compiled and minified CSS -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
					<!-- Optional theme -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
				</head>
				<body>
				<br/>
				<div class="well col-md-6 col-md-offset-3">
				<p>Vous êtes connecté !</p>
				</div>
				<!-- Latest compiled and minified JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				</body>
				</html>';
				
				// On retourne dans nasa.php après deux sec
				header('Refresh:2;url=../nasa.php');
			}
		}
	 }
?>