<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8"/>
	<title>Xfiles - Homepage</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>

<?php 
	
	// On se connecte à la bdd
	include('config/database.php');
	
	// On démarre la session
	session_start();
	// Si id et pseudo sont valides alors ....
	if(isset($_SESSION['id']) AND isset($_SESSION['pseudo'])){
		// On affiche bonjour + le nom de l'utilisateur
		echo 'Bonjour ' .$_SESSION['pseudo'];
		//Redirection après deux secondes sur homepage.php
			header('Refresh:3;url=views/homepage.php');
	}


?>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
</body>
</html>