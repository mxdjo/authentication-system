<?php

	// On se connecte à la bdd et on démarre la session
	include('config/database.php');
	session_start();
	// Suppression des variables de session et de la session
	$_SESSION = array();
	session_destroy();
	// Suppression des cookies de connexion automatique
	setcookie('login','');
	setcookie('pass_hache', '');

	echo '
	<html>
	<head>
	<meta charset="utf-8"/>
	<title>Nasa</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	</head>
	<body>
	<br/>
	<div class="well col-md-6 col-md-offset-3">
	<p>	Vous etes déconnecté</p>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</body>
	</html>
	';
	
	// On retourne sur index.php après deux secondes
	header('Refresh:2;url=index.php');
?>